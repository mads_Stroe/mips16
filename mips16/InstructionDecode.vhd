----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/08/2020 06:05:29 PM
-- Design Name: 
-- Module Name: InstructionDecode - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity InstructionDecode is
 Port (
 RegWrite: in std_logic;
 Instr: in std_logic_vector(15 downto 0);
 RegDst: in std_logic;
 clk: in std_logic;
 RD1: out std_logic_vector(15 downto 0);
 RD2: out std_logic_vector(15 downto 0);
 WD: in std_logic_vector(15 downto 0);
 ExtOp: in std_logic;
 Ext_imm: out std_logic_vector(15 downto 0);
 func: out std_logic_vector(2 downto 0);
 sa: out std_logic;
 en: in std_logic --pt scrierea in blocul de registre, acelasi enable ca la scriere PC in IF
  );
end InstructionDecode;

architecture Behavioral of InstructionDecode is

type memory is array(0 to 7) of std_logic_vector(15 downto 0);


--signal mem: memory:=( x"0001", x"0010",x"0310",x"6010",others => x"0000");
signal mem: memory;
signal WriteAddress: std_logic_vector(2 downto 0):="000";
signal ra1: std_logic_vector(2 downto 0):="000";
signal ra2: std_logic_vector(2 downto 0):="000";
begin

func<=Instr(2 downto 0);
sa<=Instr(3);
--primii 3 biti sunt pentru opcode
ra1<=Instr(12 downto 10);
ra2<=Instr(9 downto 7);

--Write address este adresa registrului �n care se scrie valoarea de pe
--intrarea de date Write data, RegWrite este 1

process(clk)
begin
    if rising_edge(clk) then
        if en='1' and RegWrite='1' then
            mem(conv_integer(WriteAddress))<=WD;
        end if;
    end if;
end process;

--primii 3 biti sunt pentru opcode, urmatorii 3 sunt scrisi pe WA daca RegDst='0' altfel sunt scrisi cei de pe pozitiile 6 5 4
process(RegDst,Instr)
begin
    if RegDst='0' then
        WriteAddress<=Instr(9 downto 7);
    else
        WriteAddress<=Instr(6 downto 4);
   end if;
end process;

--pentru extinderea semnului, in cazul in care ultimul bit adica 6 este 1 si extOp='1', extindem cu semn, adica 1
--altfel, daca cel putin una din conditiile de mai sus nu e respectata, extindem cu 0
process(ExtOp,Instr)
begin
    if ExtOp='1' and Instr(6)='1' then
        Ext_imm<="111111111"&Instr(6 downto 0);
    else
        Ext_imm<="000000000"&Instr(6 downto 0);
    end if;
end process;

 RD1 <= mem(conv_integer(ra1));
 RD2 <= mem(conv_integer(ra2));   
 


end Behavioral;
