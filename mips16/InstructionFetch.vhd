----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/08/2020 02:50:36 PM
-- Design Name: 
-- Module Name: InstructionFetch - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity InstructionFetch is
    Port ( clk : in STD_LOGIC;
           Jump : in STD_LOGIC;
           PCSrc : in STD_LOGIC;
           JumpAdress : in STD_LOGIC_VECTOR (15 downto 0);
           BranchAdress : in STD_LOGIC_VECTOR (15 downto 0);
           nextInst : out STD_LOGIC_VECTOR (15 downto 0);
           en : in STD_LOGIC;
           reset: in std_logic;
           instruction : out STD_LOGIC_VECTOR (15 downto 0));
end InstructionFetch;

architecture Behavioral of InstructionFetch is

type ROM_mem is array(0 to 16) of std_logic_vector(15 downto 0);
signal ROM : ROM_mem  := (

B"001_000_010_0000101",   --X"2105"  --addi $2,$0,5    Initializam R2 cu 5
B"001_000_011_0000100",   --X"2184"  --addi $3,$0,4    Initializam R3 cu 4
B"001_000_100_0000100",   --X"2204"  --addi $4,$0,4    Initializam R4 cu 4
B"000_011_011_010_0_000",--X"0dA0"   --add $2,$3,$3
B"001_000_101_0000001",   --X"2281"  --addi $5,$0,1    Initializam R5 cu 1
B"000_010_101_010_0_001",-- x"0AA1" --sub $2,$2,$5 R2 ar trebui sa aiba valoarea 7 acum
--salvam in memorie valoarea lui R2 ca sa verificam functionalitatea instructiunilor sw si lw
B"011_000_010_1010000", --X"6150" sw $2, result($0) adica 80($0) ar trebui sa avem 7
B"010_000_111_1010000",   --X"43D0"  --lw $7,80($0)  luam valoarea din memorie si o salvam in R7
B"000_111_101_111_0_001",-- x"1EF1" --sub $7,$7,$5 R2 ar trebui sa aiba valoarea 6 acum
B"000_111_111_111_0_000",--X"1FF0"   --add $7,$7,$7 avem 12(C)
B"001_000_100_0001111",   --X"220d"  --addi $4,$0,15    Initializam R4 cu 15
B"100_100_111_0000010",   --X"9382"  --beq $4,$7,2
--aici am terminat verificarea
B"000_100_101_100_0_001",-- x"12C1" --sub $4,$4,$5 R4 ar trebui sa scada cu 1
B"111_0000000001011", --x"E00b" j 11 --sarim beq
B"000_010_111_110_0_101", -- x"0bE5" or $6,$2,$7 facem 0111 or 1100 -> 1111
B"011_001_110_1010000", --X"6750" sw $6, result($1) adica 80($1)
B"111_0000000000000", --x"E000" j 0 --sarim la prima instructiune, adica instructiunea cu index 0

--punem 0 in registru daca facem xor cu el insusi


others=>x"0000"
); 
signal counter: std_logic_vector(15 downto 0):=x"0000";
signal nextInstruction:std_logic_vector(15 downto 0):=x"0000";
signal mux1:std_logic_vector(15 downto 0):=x"0000";
begin

process(Jump)
begin
    if Jump='1' then
        nextInstruction<=JumpAdress;
    else
        nextInstruction<=mux1;
    end if;
end process;

process(PCSrc)
begin
    if PCSrc='1' then
        mux1<=BranchAdress;
    else
        mux1<=counter+x"0001"; -- PC <- PC + 4
   end if;
end process;

process(clk)
begin
    if rising_edge(clk) then
        if en='1' then
            counter<=nextInstruction;
        elsif reset='1' then
            counter<=x"0000";
        end if;
    end if;
end process;

nextInst<=counter+1;
instruction<=ROM(conv_integer(counter(7 downto 0)));

end Behavioral;

--Raport Instruction Fetch
--Avem mai sus cateva instructiuni scrise in memorie.
--Cand sw(7)='1' se va afisa adresa instructiunii urmatoare, altfel, se va afisa numarul instructiunii curente.
--In cazul in care sw(0)='1', care este folosit pentru semnalul de Jump, urmatoarea instructiune va fi JumpAddress.
--Daca este sw(0)='0', va fi afisat rezultatul de pe mux1, care este de fapt BranchAddress daca sw(1)='1', altfel este doar urmatoarea instructiune din memorie.
