----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/08/2020 10:22:36 PM
-- Design Name: 
-- Module Name: UnitateMemorie - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity UnitateMemorie is
  Port (
  MemWrite: in std_logic;
  ALUResIn: in std_logic_vector(15 downto 0);
  RD2: in std_logic_vector(15 downto 0);
  clk: in std_logic;
  en: in std_logic;
  MemData: out std_logic_vector(15 downto 0);
  ALUResOut: out std_logic_vector(15 downto 0)
   );
end UnitateMemorie;

architecture Behavioral of UnitateMemorie is

type mem_type is array (0 to 7) of STD_LOGIC_VECTOR(15 downto 0);
signal mem : mem_type;

begin

process(clk)
begin
    if rising_edge(clk) then
        if en='1' and MemWrite='1' then
            mem(conv_integer(ALUResIn(4 downto 0)))<=RD2;
         end if;
    end if;
end process;

MemData <= mem(conv_integer(ALUResIn(4 downto 0)));
ALUResOut <= ALUResIn;

end Behavioral;
