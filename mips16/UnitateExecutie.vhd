----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/08/2020 09:25:34 PM
-- Design Name: 
-- Module Name: UnitateExecutie - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity UnitateExecutie is
 Port ( 
 nextAddress: in std_logic_vector(15 downto 0);
 RD1: in std_logic_vector(15 downto 0);
 RD2: in std_logic_vector(15 downto 0);
 Ext_imm: in std_logic_vector(15 downto 0);
 sa: in std_logic;
 func: in std_logic_vector(2 downto 0);
 ALURes: out std_logic_vector(15 downto 0);
 ALUSrc: in std_logic;
 ALUOp: in std_logic_vector(2 downto 0);
 Zero: out std_logic;
 BranchAddress: out std_logic_vector(15 downto 0)
 );
end UnitateExecutie;

architecture Behavioral of UnitateExecutie is
signal inALU2: std_logic_vector(15 downto 0);
signal ALUCtrl: std_logic_vector(2 downto 0);
signal result: std_logic_vector(15 downto 0);


begin
BranchAddress<=nextAddress+Ext_imm;

process(ALUSrc)
begin
    if ALUSrc='0' then
        inALU2<=RD2;
    elsif ALUSrc='1' then
        inALU2<=Ext_imm;
    end if;
end process;

    --codificare semnal ALUCtrl
    --si aici vorbim de fapt de opcode
    process(ALUOp)
    begin
        case ALUOp is 
            when "000" => ALUCtrl<="000";
                case func is
                    when "000" => ALUCtrl <= "000"; --add
                    when "001" => ALUCtrl <= "001"; --sub
                    when "010" => ALUCtrl <= "010"; --sll
                    when "011" => ALUCtrl <= "011"; --srl
                    when "100" => ALUCtrl <= "100"; --and
                    when "101" => ALUCtrl <= "101"; --or
                    when "110" => ALUCtrl <= "110"; --xor
                    when others => ALUCtrl <= (others => '0');
                end case;
            when "001" => ALUCtrl<="000"; --+ instructiunea addi/lw/sw
            when "010" => ALUCtrl<="001"; -- - beq
            when "101" => ALUCtrl<="100"; -- & 
            when "110" => ALUCtrl<="101"; -- | 
            --nu am prea inteles partea cu instructiunile de tip I
            when others => ALUCtrl <= (others => '0');
        end case;
    end process;
    
    process(ALUCtrl)
    begin
         case ALUCtrl is
            when "000" => result <= RD1+ inALU2;
            when "001" => result <=RD1- inALU2;
            when "010" => result <=RD1- inALU2; --pentru beq
            when "100" => result <=RD1 and inALU2;
            when "101" => result <= RD1 or inALU2;
            when others => result <= (others => '0');
         end case;
        
    end process;
    
process(result)
begin
    case result is
        when x"0000" => Zero <= '1';
        when others => Zero <= '0';
end case;
      
end process;
    
    ALURes<=result;

end Behavioral;
