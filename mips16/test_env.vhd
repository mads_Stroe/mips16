----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07.03.2020 15:56:15
-- Design Name: 
-- Module Name: test_env - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity test_env is
  Port ( clk : in STD_LOGIC;
           btn : in STD_LOGIC_VECTOR (4 downto 0);
           sw : in STD_LOGIC_VECTOR (15 downto 0);
           led : out STD_LOGIC_VECTOR (15 downto 0);
           an : out STD_LOGIC_VECTOR (3 downto 0);
           cat : out STD_LOGIC_VECTOR (6 downto 0));
          
end test_env;

architecture Behavioral of test_env is
component SSD is
 Port ( Digit: in std_logic_vector(15 downto 0);
       clk: in std_logic;
        an: out std_logic_vector(3 downto 0);
       cat: out std_logic_vector(6 downto 0));
end component;

component MPG is
  Port ( en: out STD_LOGIC;
        input: in STD_LOGIC;
        clock: in STD_LOGIC);
end component;

component InstructionFetch is
    Port ( clk : in STD_LOGIC;
           Jump : in STD_LOGIC;
           PCSrc : in STD_LOGIC;
           JumpAdress : in STD_LOGIC_VECTOR (15 downto 0);
           BranchAdress : in STD_LOGIC_VECTOR (15 downto 0);
           nextInst : out STD_LOGIC_VECTOR (15 downto 0);
           en : in STD_LOGIC;
           reset: in std_logic;
           instruction : out STD_LOGIC_VECTOR (15 downto 0));
end component;

component InstructionDecode is
 Port (
 RegWrite: in std_logic;
 Instr: in std_logic_vector(15 downto 0);
 RegDst: in std_logic;
 clk: in std_logic;
 RD1: out std_logic_vector(15 downto 0);
 RD2: out std_logic_vector(15 downto 0);
 WD: in std_logic_vector(15 downto 0);
 ExtOp: in std_logic;
 Ext_imm: out std_logic_vector(15 downto 0);
 func: out std_logic_vector(2 downto 0);
 sa: out std_logic;
 en: in std_logic --pt scrierea in blocul de registre, acelasi enable ca la scriere PC in IF
  );
end component;



component MainControl is
 Port (Instr: in std_logic_vector(15 downto 0);
       RegDst:out std_logic;
       ExtOp: out std_logic;
       ALUSrc: out std_logic;
       Branch: out std_logic;
       Jump: out std_logic;
       ALUOp: out std_logic_vector(2 downto 0);
       MemWrite: out std_logic;
       MemtoReg: out std_logic;
       RegWrite: out std_logic
  );
end component;


component UnitateMemorie is
  Port (
  MemWrite: in std_logic;
  ALUResIn: in std_logic_vector(15 downto 0);
  RD2: in std_logic_vector(15 downto 0);
  clk: in std_logic;
  en: in std_logic;
  MemData: out std_logic_vector(15 downto 0);
  ALUResOut: out std_logic_vector(15 downto 0)
   );
end component;

component UnitateExecutie is
 Port ( 
 nextAddress: in std_logic_vector(15 downto 0);
 RD1: in std_logic_vector(15 downto 0);
 RD2: in std_logic_vector(15 downto 0);
 Ext_imm: in std_logic_vector(15 downto 0);
 sa: in std_logic;
 func: in std_logic_vector(2 downto 0);
 ALURes: out std_logic_vector(15 downto 0);
 ALUSrc: in std_logic;
 ALUOp: in std_logic_vector(2 downto 0);
 Zero: out std_logic;
 BranchAddress: out std_logic_vector(15 downto 0)
 );
end component;



signal temp: std_logic_vector(15 downto 0);
signal en,reset: std_logic;
signal resALU: std_logic_vector(15 downto 0);
signal currentIn:std_logic_vector(15 downto 0):=x"0000";
signal nextIn:std_logic_vector(15 downto 0):=x"0000";
--signal print:std_logic_vector(15 downto 0):=x"0000";
signal RegWrite,RegDst,ExtOP,Branch,Jump,MemWrite,MemtoReg,ALUSrc:std_logic;
signal ALUOp: std_logic_vector(2 downto 0):="000";
signal rd1: std_logic_vector(15 downto 0):=x"0000";
signal rd2: std_logic_vector(15 downto 0):=x"0000";
signal sum: std_logic_vector(15 downto 0):=x"0000";
signal func:std_logic_vector(2 downto 0):="000";
signal sa: std_logic;
signal Ext_imm: std_logic_vector(15 downto 0):=x"0000";
signal wb: std_logic_vector(15 downto 0):=x"0000";
signal ALUResOut: std_logic_vector(15 downto 0):=x"0000";
signal MemData: std_logic_vector(15 downto 0):=x"0000";
signal Zero: std_logic;
signal PCSrc: std_logic;
signal JumpAddress:std_logic_vector(15 downto 0):=x"0000";
signal BranchAddress:std_logic_vector(15 downto 0):=x"0000";

begin 

afisor: SSD port map(Digit=>temp,clk=>clk,an=>an,cat=>cat);
generator: MPG port map(en=>en,input=>btn(0),clock=>clk);
rst: MPG port map(en=>reset,input=>btn(1),clock=>clk);

instrFetch: InstructionFetch port map(clk=>clk,Jump=>Jump,PCSrc=>PCSrc,JumpAdress=>JumpAddress,BranchAdress=>BranchAddress,nextInst=>nextIn,en=>en,reset=>reset,instruction=>currentIn);
instrDecode: InstructionDecode port map(RegWrite=>RegWrite,Instr=>currentIn,RegDst=>RegDst,clk=>clk,RD1=>rd1,Rd2=>rd2,WD=>wb,ExtOp=>ExtOp,Ext_imm=>Ext_imm,func=>func,sa=>sa,en=>en);
UC:MainControl port map(Instr=>currentIn,RegDst=> RegDst, ExtOp=>ExtOp,ALUSrc=>ALUSrc, Branch=>Branch,Jump=>Jump,ALUOp=>ALUOp,MemWrite=>MemWrite,MemtoReg=>MemtoReg, RegWrite=>RegWrite);
memorie: UnitateMemorie port map(MemWrite=>MemWrite,ALUResIn=>resALU,RD2=>rd2,clk=>clk,en=>en,MemData=>MemData,ALUResOut=>ALUResOut);
EX: UnitateExecutie port map(nextAddress=>nextIn,RD1=>rd1,RD2=>rd2,Ext_imm=>Ext_imm,sa=>sa,func=>func,ALURes=>resALU,ALUSrc=>ALUSrc,ALUOp=>ALUOp,Zero=>Zero,BranchAddress=>BranchAddress);

sum<=rd1+rd2;

--temp <= currentIn when sw(7)='1' else nextIn;

led(0)<= RegWrite;
led(1)<= MemtoReg;
led(2)<=MemWrite;
led(3)<= Jump;
led(4)<=Branch;
led(5)<=ALUSrc;
led(6)<=ExtOp;
led(7)<=RegDst;
led(10 downto 8)<=ALUOp;

 with sw(7 downto 5) select
            temp<= currentIn when "000",
                   nextIn when "001",
                   rd1 when "010",
                   rd2 when "011",
                   Ext_imm when "100", 
                   ALUResOut when "101", 
                   MemData when "110", 
                   wb when "111";

--pentru MUX
process(MemtoReg)
begin
    if MemtoReg='0' then
        wb<=ALUResOut;
    else
        wb<=MemData;
    end if;
end process;

--pentru Branch
PCSrc <= Branch and Zero;

--pentru Jump
process(Jump)
begin
    if Jump='1' then
        JumpAddress<=nextIn(15 downto 13) & currentIn(12 downto 0);
    end if;
end process;

end Behavioral;
